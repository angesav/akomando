const path = require('path');
const libraryName = 'akomando';

module.exports = {
   entry: path.resolve(__dirname, 'src/api/akomando.js'),
   output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.akomando.js',
      library: libraryName,
      libraryTarget: 'umd',
      umdNamedDefine: true
   },

   mode: 'production',
   target: 'node',

   module: {
      rules: [{
         test: /\.js$/,
         include: path.resolve(__dirname, 'src'),
         use: [{
            loader: 'babel-loader'
         }]
      }]
   }
};
