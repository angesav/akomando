function getFile(name, callback) {
   if (isNode()) {
      getFileNode(name, callback);
   } else {
      getFileBrowser(name, callback);
   }
}

function isNode() {
   return typeof process !== 'undefined' &&
            process.versions != null &&
            process.versions.node != null;
}

function getFileNode(name, callback) {
   const fs = require('fs');
   return fs.readFile('test/docs/'+name, (err, data) => {
      if (err) throw err;
      callback(data.toString());
   });
}

function getFileBrowser(name, callback) {
   $.get('/test/docs/'+name, (data) => {
      callback(data);
   }, 'text');
}

function createAkomando() {
   if (isNode()) {
      const akomando = require('../../src/api/akomando');
      return akomando.createAkomando.apply(null, arguments);
   } else {
      // akomando should be loaded by test-browser.html
      return window.akomando.createAkomando.apply(null, arguments);
   }
}

export {
   getFile,
   createAkomando
}
