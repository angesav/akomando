import { expect } from 'chai';
import { getFile, createAkomando } from '../imports/utils'

describe('#AkomantosoReferencesHandler Document document_it.xml (Italy)', function () {
   let myAkomando;
   before((done) => {
      getFile('document_it.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('It has to find 91 references', function () {
      const n = myAkomando.getReferencesInfo().references.number;
      expect(n).to.equal(91);
   });

   it('The length of the references list has to be equal to the "numer" field ', function () {
      const ref = myAkomando.getReferencesInfo({filterBy:'all'}).references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      const tot = myAkomando.getReferencesInfo({ filterBy: 'all' }).references.list.length;
      const used = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list.length;
      const unused = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list.length;
      expect(used + unused).to.equal(tot);
   });

   it('The id of the first unused reference has to be "ra1" ', function () {
      const eId = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list[0].eId;
      expect(eId).to.equal('ra1');
   });

   it('The ref of the first unused reference has to be "/akn/it/act/legge/stato/2014-04-28/67" ', function () {
      const href = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list[0].href;
      expect(href).to.equal('/akn/it/act/legge/stato/2014-04-28/67');
   });

   it('The showas of the last unused reference has to be "NIR" ', function () {
      const showAs = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list[12].showAs;
      expect(showAs).to.equal('NIR');
   });

});

describe('#AkomantosoReferencesHandler Document document_uy.xml (Uruguay)', function () {
   let myAkomando;
   before((done) => {
      getFile('document_uy.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('It has to find 7 references', function () {
      const n = myAkomando.getReferencesInfo().references.number;
      expect(n).to.equal(7);
   });

   it('The length of the references list has to be equal to the "numer" field ', function () {
      const ref = myAkomando.getReferencesInfo({ filterBy: 'all' }).references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      const tot = myAkomando.getReferencesInfo({ filterBy: 'all' }).references.list.length;
      const used = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list.length;
      const unused = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list.length;
      expect(used + unused).to.equal(tot);
   });

   it('The id of the first used reference has to be "limeEditor" ', function () {
      const eId = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list[0].eId;
      expect(eId).to.equal('limeEditor');
   });

   it('The ref of the first used reference has to be "/lime.cirsfid.unibo.it" ', function () {
      const href = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list[0].href;
      expect(href).to.equal('/lime.cirsfid.unibo.it');
   });

   it('The showas of the last used reference has to be "Editor" ', function () {
      const showAs = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list[6].showAs;
      expect(showAs).to.equal('Editor');
   });
});

describe('#AkomantosoReferencesHandler document_unqualified.xml(Italy and unqualified elements)', function () {
   let myAkomando;
   before((done) => {
      getFile('document_unqualified.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('It has to find 91 references', function () {
      const n = myAkomando.getReferencesInfo().references.number;
      expect(n).to.equal(91);
   });

   it('The length of the references list has to be equal to the "numer" field ', function () {
      const ref = myAkomando.getReferencesInfo({ filterBy: 'all' }).references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      const tot = myAkomando.getReferencesInfo({ filterBy: 'all' }).references.list.length;
      const used = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list.length;
      const unused = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list.length;
      expect(used + unused).to.equal(tot);
   });

   it('The id of the first unused reference has to be "ra1" ', function () {
      const eId = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list[0].eId;
      expect(eId).to.equal('ra1');
   });

   it('The ref of the first unused reference has to be "/akn/it/act/legge/stato/2014-04-28/67" ', function () {
      const href = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list[0].href;
      expect(href).to.equal('/akn/it/act/legge/stato/2014-04-28/67');
   });

   it('The showas of the last unused reference has to be "NIR" ', function () {
      const showAs = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list[12].showAs;
      expect(showAs).to.equal('NIR');
   });
});


describe('#AkomantosoReferencesHandler document_qualifiedWithPrefix.xml (Italy and qualified elements)', function () {
   let myAkomando;
   before((done) => {
      getFile('document_qualifiedWithPrefix.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('It has to find 91 references', function () {
      const n = myAkomando.getReferencesInfo().references.number;
      expect(n).to.equal(91);
   });

   it('The length of the references list has to be equal to the "numer" field ', function () {
      const ref = myAkomando.getReferencesInfo({ filterBy: 'all' }).references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      const tot = myAkomando.getReferencesInfo({ filterBy: 'all' }).references.list.length;
      const used = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list.length;
      const unused = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list.length;
      expect(used + unused).to.equal(tot);
   });

   it('The id of the first unused reference has to be "ra1" ', function () {
      const eId = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list[0].eId;
      expect(eId).to.equal('ra1');
   });

   it('The ref of the first unused reference has to be "/akn/it/act/legge/stato/2014-04-28/67" ', function () {
      const href = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list[0].href;
      expect(href).to.equal('/akn/it/act/legge/stato/2014-04-28/67');
   });

   it('The showas of the last unused reference has to be "NIR" ', function () {
      const showAs = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list[12].showAs;
      expect(showAs).to.equal('NIR');
   });
});

describe('#AkomantosoReferencesHandler Document document_docCollection.xml(Uruguaian qualified document collection)', function () {
   let myAkomando;
   before((done) => {
      getFile('document_docCollection.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('It has to find 9 references', function () {
      const n = myAkomando.getReferencesInfo().references.number;
      expect(n).to.equal(9);
   });

   it('The length of the references list has to be equal to the "numer" field ', function () {
      const ref = myAkomando.getReferencesInfo({ filterBy: 'all' }).references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      const tot = myAkomando.getReferencesInfo({ filterBy: 'all' }).references.list.length;
      const used = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list.length;
      const unused = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list.length;
      expect(used + unused).to.equal(tot);
   });

   it('The id of the first used reference has to be "limeEditor" ', function () {
      const eId = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list[0].eId;
      expect(eId).to.equal('limeEditor');
   });

   it('The ref of the first used reference has to be "/lime.cirsfid.unibo.it" ', function () {
      const href = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list[0].href;
      expect(href).to.equal('/lime.cirsfid.unibo.it');
   });

   it('The showas of the last used reference has to be "Editor" ', function () {
      const showAs = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list[3].showAs;
      expect(showAs).to.equal('Editor');
   });
});

describe('#AkomantosoReferencesHandler Document document_docCollection_componentsEverywhere.xml ', function () {
   let myAkomando;
   before((done) => {
      getFile('document_docCollection_componentsEverywhere.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('It has to find 9 references', function () {
      const n = myAkomando.getReferencesInfo().references.number;
      expect(n).to.equal(9);
   });

   it('The length of the references list has to be equal to the "numer" field ', function () {
      const ref = myAkomando.getReferencesInfo({ filterBy: 'all' }).references;
      expect(ref.number).to.equal(ref.list.length);
   });

   it('The total number of references has to be equal to the sum used and unused ones', function () {
      const tot = myAkomando.getReferencesInfo({ filterBy: 'all' }).references.list.length;
      const used = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list.length;
      const unused = myAkomando.getReferencesInfo({ filterBy: 'unused' }).references.list.length;
      expect(used + unused).to.equal(tot);
   });

   it('The id of the first used reference has to be "limeEditor" ', function () {
      const eId = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list[0].eId;
      expect(eId).to.equal('limeEditor');
   });

   it('The ref of the first used reference has to be "/lime.cirsfid.unibo.it" ', function () {
      const href = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list[0].href;
      expect(href).to.equal('/lime.cirsfid.unibo.it');
   });

   it('The showas of the last used reference has to be "Editor" ', function () {
      const showAs = myAkomando.getReferencesInfo({ filterBy: 'used' }).references.list[3].showAs;
      expect(showAs).to.equal('Editor');
   });
});
