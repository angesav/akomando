// utils
import { _serializeNodes } from '../constants/utils/utils.js';


// classes
import _QueryHandler from './query-handler.js';


/**
 * A class to handle all features related to metadata of AkomaNtoso.
 * The class can be used for the following:
 *
 * - to request meta elements in the AkomaNtoso documents as XML Node,
 *   XML String, HTML Document, HTMLString, JSON and JSONString
*/
export default class _MetaHandler {
   /**
    * Create an AkomaNtoso meta handler.
   */
   constructor(){
   }

   /**
    * Get the requested metadata in the requested serialization. If no metadata is specified it returns the whole meta block.
    * @param {Object} options Options for the request and the found meta elements
    * @param {String} [options.serialize=AKNDom] - The serialization of the returning meta. Possible values are:
    * - AKNDom - returns the XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    *     Document</a> serialization of requested meta
    * - AKNString - returns the String serialization of requested meta
    * - HTMLDom - returns the HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    *     Document</a> serialization of requested meta
    * - HTMLString - returns the HTML string serialization of requested meta
    * - JSON - returns the <a target="_blank" href="https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/JSON">
    *     JSON</a> serialization of requested meta
    * - JSONString - returns the JSON string serialization of requested meta
    * @param {String} [options.namespace=null] - The namespace in which metadata must be searched
    * @param {String} [options.prefix=null] - The prefix of the metadata that must be found
    * @param {String} [options.metaRoot=meta] - Meta elements that must be found
    * @param {String} [options.insertDataXPath=false] - Set it to true to add the data-akomando-xpath attribute to each of the returned elements
    * @param {String} [options.addHtmlElement=true] - Set it to true to add the html element into the created document (if the HTML serialization is required)
    * @param {String} [options.addHeadElement=true] - Set it to true to add the head element into the created document (if the HTML serialization is required)
    * @param {String} [options.addBodyElement=true] - Set it to true to add the body element into the created document (if the HTML serialization is required)
    * @param {Document} [options.AKNDom=null] - The XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    * Document</a> serialization of the AkomaNtoso in which the research must be performed
    * @param {Object} [options.config] The configuration of the akomando according to {@link akomando.config} specications.
    * @return {Array} An array containing all meta in the requested serialization cointained in the AkomaNtoso document
   */
   static getMeta({
      serialize = 'AKNDom',
      namespace = null,
      prefix = null,
      metaRoot = null,
      AKNDom = null,
      insertDataXPath,
      addHtmlElement,
      addHeadElement,
      addBodyElement,
      config,
   } = {}){

      const queryHandler = new _QueryHandler({
         config,
      });

      var nodes = queryHandler.selectMeta(namespace, prefix, metaRoot, AKNDom);

      const results = _serializeNodes(
         nodes,
         serialize, {
            insertDataXPath,
            addHtmlElement,
            addHeadElement,
            addBodyElement,
         }
      );
      return results;
   }
}
