// utils
import { _stripPrefix } from '../constants/utils/utils.js';

/**
 * A class to build proper query according to a given configuration. If no configuration are set it uses the ones in
 * /config/config.json.
 * The class can be used for the following:
 *
 * - to build a query for retrieving all the docTypes in AkomaNtoso documents
 * - to build a query for retrieving metadata in AkomaNtoso documents
*/
export default class _QueryBuilder {
   /**
    * Create an _QueryBuilder configured with the given configuration.
    * @param {Object} options Options for the request and the found meta elements
    * @param {Object} [options.config] The configuration of the akomando according to {@link akomando.config} specications.
    */
   constructor({
      config,
   } = {}){
      /**
       * @property {Object} _config Stores the configurations of the query builder
      */
      this._config = config;
   }

   /**
    * Builds a query string to retrieve all the docTypes in AkomaNtoso documents
    * @param {String} prefix The prefix that must be prepended to the query.
    * @return {String} A string containing the access point to all the docuements types in the given configuration
   */
   buildAllDocTypesQuery(prefix=null){
      let returnQuery = `${prefix
         ?
         `${prefix}:${this._config.akomaNtosoRoot}`
         :
         this._config.akomaNtosoRoot}/*[`;
      for (const docType in this._config.docTypes){
         returnQuery = `${returnQuery}${prefix
            ?
            `self::${prefix}:${this._config.docTypes[docType].name}`
            :
            `self::${this._config.docTypes[docType].name}`}|`;
      }
      return `${returnQuery.substring(0, returnQuery.length - 1)}]`;
   }

   /**
    * Builds a query string to retrieve all the components in AkomaNtoso documents
    * @param {String} prefix The prefix that must be prepended to the query.
    * @return {String} A string containing the access point to all the components in the given configuration
   */
   buildComponentsQuery(prefix=null){
      let returnQuery = `${prefix
         ?
         `${prefix}:${this._config.akomaNtosoRoot}/${prefix}:${this._config.components.name}/${prefix}:${this._config.component.name}`
         :
         `${this._config.akomaNtosoRoot}/${this._config.components.name}/${this._config.component.name}`}/*[`;
      for (const docType in this._config.docTypes){
         returnQuery = `${returnQuery}${prefix
            ?
            `self::${prefix}:${this._config.docTypes[docType].name}`
            :
            `self::${this._config.docTypes[docType].name}`}|`;
      }
      return `${returnQuery.substring(0, returnQuery.length - 1)}]`;
   }

   /**
    * Builds the query to retrieve metadata in AkomaNtoso documents.
    * @param {String} aMetaRoot The name of the meta that must be retrieved (can be null).
    * @param {String} prefix The prefix that must be prepended to the query (can be null).
    * @return {String} The query for retrieving requested meta elements
   */
   buildMetaQuery(aMetaRoot, prefix=null){
      const metaRoot = _stripPrefix(aMetaRoot);
      const requiredMetaRoot =
      (metaRoot == this._config.meta.name)
      ?
         `${prefix
            ?
            `${prefix}:${this._config.meta.name}`
            :
            this._config.meta.name}`
      :
         `${prefix
            ?
            `${prefix}:${this._config.meta.name}//${prefix}:${metaRoot}`
            :
            `${this._config.meta.name}//${metaRoot}`}`;
      return `${this.buildAllDocTypesQuery(prefix)}/${requiredMetaRoot}|${this.buildComponentsQuery(prefix)}/${requiredMetaRoot}`;
   }
}
