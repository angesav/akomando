// libs
import { DOMParser } from 'xmldom';
import { typeCheck } from 'type-check';
import { pd } from 'pretty-data2';
import { clean } from 'underscore.string';
import _ from 'underscore';

// utils
import { _transformDomToHTML,
         _createHTMLDocument,
         _serializeDomToString,
         _transformDomToJSON,
         _stripPrefix,
         _getDocumentElementsIdentifier,
    } from '../constants/utils/utils.js';

// configs
import aknConfig from '../config/config.json';

// errors
import errors from '../config/errors.json';

// classes
import _MetaHandler from './meta-handler.js';
import _ComponentsHandler from './components-handler.js';
import _HiersHandler from './hiers-handler.js';
import _ReferencesHandler from './references-handler.js';

/**
 * This Class is used to handle AkomaNtoso documents.
*/
export default class _DocumentHandler {
   /**
    * Create an AkomantosoDocumentHandler.
    * @param {Object} options Options for building the _DocumentHandler
    * @param {Object} [options.config] The configuration of the akomando according to {@link akomando.config} specications.
    *
   */
   constructor({
      config = aknConfig,
   } ={}){
      /**
       * @property (String) _AKNConfig a config string according to {@link akomando.config} specications.
      */
      this._AKNConfig = (config !== aknConfig) ? JSON.parse(config) : config;
      /**
       * @property {Object} _AKNDom Stores the XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
       * @property {DOM} _AKNDom.mainDocument Stores the XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
       * Document</a> serialization of the loaded AkomaNtoso document
       * @property {Object} _AKNDom.components an object containing the components of the akomantoso document structured as {@link akomando.components.getComponents}
       * @property {Object} _AKNDom.attachments an object containing the attachments of the akomantoso document structured as {@link akomando.attachments.getAttachments}
      */
      this._AKNDom = {
         mainDocument: null,
         components: null,
         attachments: null,
      };
      /**
       * @property {String} _AKNDomNamespace Stores the namespace of the loaded AkomaNtoso document
      */
      this._AKNDomNamespace = null;
      /**
       * @property {String} _AKNDomDocType Stores the doc type of the loaded AkomaNtoso document
      */
      this._AKNDomDocType = null;
      /**
       * @property {String} _AKNDomPrefix Stores the prefix of the loaded AkomaNtoso document
      */
      this._AKNDomPrefix = null;
      /**
       * @property {String} _AKNString Stores the XML String serialization of the loaded AkomaNtoso document
      */
      this._AKNString = null;

      /**
       * @property {String} _HTMLDom Stores the HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
       *   Document</a> serialization of the loaded AkomaNtoso document
      */
      this._HTMLDom = null;
      /**
       * @property {String} _HTMLString Stores the HTML String serialization of the loaded AkomaNtoso document
      */
      this._HTMLString = null;
      /**
       * @property {String} _AKNJSON Stores the JSON serialization of the loaded AkomaNtoso document
      */
      this._AKNJSON = null;
      /**
       * @property {String} _AKNJSONString Stores the JSON String serialization of the loaded AkomaNtoso document
      */
      this._AKNJSONString = null;
   }

   /**
    * Private method to transofrm the given XML string into AKNDom
    * @return {null}
    */
   _createAknDom(){
      const parser = new DOMParser();
      this._AKNDom.mainDocument = parser.parseFromString(this._AKNString, 'application/xml');
      // here DOM for the doc collections must be created.
      /*if(this.getDocType() === this._AKNConfig.docTypes.documentCollection.name){
         //console.log(this.getDocType());
         //reeererd
      }*/
      this._AKNDomNamespace = this._AKNDom.mainDocument.documentElement.namespaceURI;
      this._AKNDomPrefix = this._AKNDom.mainDocument.documentElement.prefix;
   }

   /**
    * Private method to create the HTMLDom serialization of the AkomaNToso parsed with parseDocument
    * @param {Object} options Options for the created HTML
    * @param {String} [options.addHtmlElement=true] - Set it to true to add the html element into the created document
    * @param {String} [options.addHeadElement=true] - Set it to true to add the head element into the created document
    * @param {String} [options.addBodyElement=true] - Set it to true to add the body element into the created document
    * @return {null}
    */
   _createHTMLDom({
      addHtmlElement = true,
      addHeadElement = true,
      addBodyElement = true,
   }={}){
      const {htmlDocument, body} = _createHTMLDocument({
         addHtmlElement,
         addHeadElement,
         addBodyElement,
      });
      this._HTMLDom = _transformDomToHTML(this._AKNDom.mainDocument,body,htmlDocument);
      this._HTMLString = _serializeDomToString(this._HTMLDom);
   }

   /**
    * Private method to create the JSON serialization of the AkomaNtoso parsed with parseDocument
    * @return {null}
   */
   _createJSON(){
      this._AKNJSON = _transformDomToJSON(this._AKNString);
      this._AKNJSONString = JSON.stringify(this._AKNJSON);
   }

   /**
    * Return the AkomaNtoso document as XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    * Document</a>
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @return {Document} The AkomaNtoso document as XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    * Document</a>
    */
   _getAKNDom(){
      if(typeCheck('Undefined',this._AKNString)){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      return this._AKNDom.mainDocument;
   }

   /**
    * Return the AkomaNtoso document as String
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @return {String} The AkomaNtoso document as String
    */
   _getAKNString(){
      if(typeCheck('Undefined',this._AKNString)){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      return this._AKNString;
   }

   /**
    * Return the AkomaNtoso document as HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    * Document</a>
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @param {Object} options Options for the created HTML
    * @param {String} [options.addHtmlElement=true] - Set it to true to add the html element into the created document
    * @param {String} [options.addHeadElement=true] - Set it to true to add the head element into the created document
    * @param {String} [options.addBodyElement=true] - Set it to true to add the body element into the created document
    * @return {DomDocument} The AkomaNtoso document as <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    * Document</a>
    */
   _getHTMLDom({
      addHtmlElement = true,
      addHeadElement = true,
      addBodyElement = true,
   }={}){
      if(typeCheck('Undefined',this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      this._createHTMLDom({
         addHtmlElement,
         addHeadElement,
         addBodyElement,
      });
      return this._HTMLDom;
   }

   /**
    * Return the AkomaNtoso document as HTML String
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @param {Object} options Options for the created HTML
    * @param {String} [options.addHtmlElement=true] - Set it to true to add the html element into the created document
    * @param {String} [options.addHeadElement=true] - Set it to true to add the head element into the created document
    * @param {String} [options.addBodyElement=true] - Set it to true to add the body element into the created document
    * @return {String} The AkomaNtoso document as HTML string
    */
   _getHTMLString({
      addHtmlElement = true,
      addHeadElement = true,
      addBodyElement = true,
   }={}){
      if(typeCheck('Undefined',this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      this._createHTMLDom({
         addHtmlElement,
         addHeadElement,
         addBodyElement,
      });
      return this._HTMLString;
   }

   /**
    * Return the AkomaNtoso document as <a target="_blank" href="https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/JSON">
    * JSON</a>
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @return {JSON} The AkomaNtoso document as <a target="_blank" href="https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/JSON">
    * JSON</a>
    */
   _getJSON(){
      if(typeCheck('Undefined',this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      if (this._AKNJSON){
         return this._AKNJSON;
      }
      this._createJSON();
      return this._AKNJSON;
   }

   /**
    * Return the AkomaNtoso document as JSON String
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @return {String} The AkomaNtoso document as JSON String
    */
   _getJSONString(){
      if(typeCheck('Undefined',this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      if (this._AKNJSONString){
         return this._AKNJSONString;
      }
      this._createJSON();
      return this._AKNJSONString;
   }

   /**
    * Return the AkomaNtoso document elements as list of objects of plain text
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when serialize parameter is not a String
    * @throws {Error} Throws error when the requested serialization is not supported
    * @throws {Error} Throws error when newLines parameter is not a Boolean
    * @param {Object} options Options for the retrieved elements
    * @param {String} [options.serialize=TEXT] The serialization of the returning elements. Possible values are:
    * - TEXT - returns all elements as plain text
    * - JSON - returns a <a target="_blank" href="https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/JSON">
    * @param {Boolean} [options.newLines=true] - Set it to true to mantain the newLines in the text
    * @return {Object} The AkomaNtoso document elements in the requested serialization
    */
   _getText({
       serialize,
       newLines,
   }){
      if(typeCheck('Undefined',this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }

      if(!typeCheck('Boolean',newLines)){
         throw new Error(`newLines - ${newLines} - ${errors._InputIsNotABoolean}`);
      }

      if(!typeCheck('String',serialize)){
         throw new Error(`serialize - ${serialize} - ${errors._InputIsNotAString}`);
      }

      if(serialize !== 'JSON' &&
       serialize !== 'TEXT'){
         throw new Error(`${serialize} - ${errors._SerializationNotSupported}`);
      }
      var dupNode = this._getAKNDom().cloneNode(true);
      const metaNodes = _MetaHandler.getMeta({
         serialize: 'AKNDom',
         namespace: this.AKNDomNamespace,
         prefix: (this._AKNDomPrefix) ? this._AKNDomPrefix : 'akn',
         metaRoot: this._AKNConfig.meta.name,
         AKNDom: dupNode,
         config: this._AKNConfig,
      });
      // Remove metadata nodes
      metaNodes.forEach((node) => {
         node.parentNode.removeChild(node);
      });

      const elements = _getDocumentElementsIdentifier(dupNode);
      switch(serialize){
      case 'TEXT':
         // Return the text of the root
         if (newLines) {
            return elements[0].text;
         } else {
            return clean(elements[0].text);
         }
      default:
         return elements;
      }
   }

   /**
    * Return the Namespace URI of the loaded AkomaNtoso document
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @return {string} the namespace URI of the loaded AkomaNtos document
    */
   get AKNDomNamespace(){
      if(typeCheck('Undefined',this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      return this._AKNDomNamespace;
   }

   /**
    * Parse an XML string into an AkomaNtoso Document
    * @throws {Error} Throws error when the input is not a String
    * @param {String} XMLString - the String of the AkomaNtoso document that must be parsed
    * @return {null}
    */
   parseAKNString(XMLString){
      if (!typeCheck('String', XMLString)){
         throw new Error(errors._InputIsNotAString);
      }
      this._AKNString = pd.xmlmin(XMLString,true);
      //var metaRegex = /<body>[\w\W\s\S\d\D]*<\/body>/g;
      //this._AKNMetaString = this._AKNString.match(metaRegex).map(function (s) { return s; });
      this._createAknDom();
   }

   /**
    * Return the AkomaNtoso document type of the loaded document
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @return {String} the document type of the loaded AkomaNtoso
   */
   getDocType(){
      if(typeCheck('Undefined',this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      if(!this._AKNDomDocType){
         this._AKNDomDocType = _stripPrefix(this._AKNDom.mainDocument.documentElement.firstChild.nodeName);
      }
      return this._AKNDomDocType;
   }

   /**
    * Return the AkomaNtoso document in the requested serialization
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when serialize parameter is not a String
    * @throws {Error} Throws error when the requested serialization is not supported
    * @throws {Error} Throws error when addHtmlElement parameter is not a Boolean
    * @throws {Error} Throws error when addHeadElement parameter is not a Boolean
    * @throws {Error} Throws error when addBodyElement parameter is not a Boolean
    * @param {Object} options Options for the retrieved document
    * @param {String} [options.serialize=AKNDom] The serialization of the returning document. Possible values are:
    * - AKNDom - returns the XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    *   Document</a> serialization of the Akomantoso
    * - AKNString - returns the String serialization of the Akomantoso Document
    * - HTMLDom - returns the HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    *   Document</a> serialization of the Akomantoso
    * - HTMLString - returns the HTML string serialization of the Akomantoso
    * - JSON - returns the <a target="_blank" href="https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/JSON">
    *   JSON</a> serialization of the Akomantoso
    * - JSONString - returns the JSON string serialization of the Akomantoso
    * - TEXT - returns the plain text serialization of the Akomantoso
    * @param {String} [options.addHtmlElement=true] - Set it to true to add the html element into the created document
    * @param {String} [options.addHeadElement=true] - Set it to true to add the head element into the created document
    * @param {String} [options.addBodyElement=true] - Set it to true to add the body element into the created document
    * @param {String} [options.newLines=true] - Set it to true to mantain the newLines in the text
    * @return {Object} The AkomaNtoso document in the requested serialization
   */
   getAKNDocument({
      serialize = 'AKNDom',
      addHtmlElement = true,
      addHeadElement = true,
      addBodyElement = true,
      newLines = true,
   } = {}){
      if(typeCheck('Undefined',this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      if(!typeCheck('Boolean',addHtmlElement)){
         throw new Error(`addHtmlElement - ${addHtmlElement} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('Boolean',addHeadElement)){
         throw new Error(`addHeadElement - ${addHeadElement} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('Boolean',addBodyElement)){
         throw new Error(`addBodyElement - ${addBodyElement} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('Boolean',newLines)){
         throw new Error(`newLines - ${newLines} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('String',serialize)){
         throw new Error(`serialize - ${serialize} - ${errors._InputIsNotAString}`);
      }
      if(serialize != 'AKNDom' &&
         serialize != 'AKNString' &&
         serialize != 'HTMLDom' &&
         serialize != 'HTMLString' &&
         serialize != 'JSON' &&
         serialize != 'JSONString' &&
         serialize != 'TEXT'){
         throw new Error(`${serialize} - ${errors._SerializationNotSupported}`);
      }
      switch(serialize){
      case 'AKNDom' :
         return this._getAKNDom();
      case 'AKNString':
         return this._getAKNString();
      case 'HTMLDom':
         return this._getHTMLDom({
            addHtmlElement,
            addHeadElement,
            addBodyElement,
         });
      case 'HTMLString':
         return this._getHTMLString({
            addHtmlElement,
            addHeadElement,
            addBodyElement,
         });
      case 'JSON':
         return this._getJSON();
      case 'JSONString':
         return this._getJSONString();
      case 'TEXT':
         return this._getText({ serialize, newLines });
      default :
         return this._getAKNDom();
      }
   }

   /**
    * Get the documents metadata in the requested serialization
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when serialize parameter is not a String
    * @throws {Error} Throws error when the requested serialization is not supported
    * @throws {Error} Throws error when metaRoot parameter is not a String
    * @throws {Error} Throws error when insertDataXPath parameter is not a Boolean
    * @throws {Error} Throws error when addHtmlElement parameter is not a Boolean (if the HTML serialization is required)
    * @throws {Error} Throws error when addHeadElement parameter is not a Boolean (if the HTML serialization is required)
    * @throws {Error} Throws error when addBodyElement parameter is not a Boolean (if the HTML serialization is required)
    * @param {Object} options Options for the requested meta
    * @param {String} [options.serialize=AKNDom] The serialization of the returning meta. Possible values are:
    * - AKNDom - returns the xml <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    *     Document</a> serialization of requested meta
    * - AKNString - returns the String serialization of requested meta
    * - HTMLDom - returns the HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    *     Document</a> serialization of requested meta
    * - HTMLString - returns the HTML string serialization of requested meta
    * - JSON - returns the <a target="_blank" href="https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/JSON">
    *     JSON</a> serialization of requested meta
    * - JSONString - returns the JSON string serialization of requested meta
    *
    * @param {String} [options.metaRoot=meta] The name of meta that must be retrieved. It can be qualified or not qualified.
    * @param {String} [options.addHtmlElement=true] - Set it to true to add the html element into the created document
    * @param {String} [options.addHeadElement=true] - Set it to true to add the head element into the created document
    * @param {String} [options.addBodyElement=true] - Set it to true to add the body element into the created document
    * @return {Array} An array containing all founded meta in the requested serialization
   */
   getMeta({
      serialize = 'AKNDom',
      metaRoot = this._AKNConfig.meta.name,
      insertDataXPath = false,
      addHtmlElement = true,
      addHeadElement = true,
      addBodyElement = true,
   } = {}){
      if(typeCheck('Undefined',this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      if(!typeCheck('Boolean',insertDataXPath)){
         throw new Error(`insertDataXPath - ${insertDataXPath} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('Boolean',addHtmlElement)){
         throw new Error(`addHtmlElement - ${addHtmlElement} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('Boolean',addHeadElement)){
         throw new Error(`addHeadElement - ${addHeadElement} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('Boolean',addBodyElement)){
         throw new Error(`addBodyElement - ${addBodyElement} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('String',metaRoot)){
         throw new Error(`metaRoot - ${metaRoot} - ${errors._InputIsNotAString}`);
      }
      if(serialize != 'AKNDom' &&
         serialize != 'AKNString' &&
         serialize != 'HTMLDom' &&
         serialize != 'HTMLString' &&
         serialize != 'JSON' &&
         serialize != 'JSONString'){
         throw new Error(`${serialize} - ${errors._SerializationNotSupported}`);
      }
      const results = _MetaHandler.getMeta({
         serialize,
         namespace: this.AKNDomNamespace,
         prefix: (this._AKNDomPrefix) ? this._AKNDomPrefix : 'akn',
         metaRoot,
         insertDataXPath,
         AKNDom: this._getAKNDom(),
         config: this._AKNConfig,
         addHtmlElement,
         addHeadElement,
         addBodyElement,
      });
      return results;
   }

   /**
     * Returns the components contained in the document in an object structured as {@link akomando.components.getComponents}
     * @throws {Error} Throws error when the AkomaNtoso document is not set
     * @throws {Error} Throws error when serialize parameter is not a String
     * @throws {Error} Throws error when the requested serialization is not supported
     * @throws {Error} Throws error when detailedInfo parameter is not a Boolean
     * @throws {Error} Throws error when insertDataXPath parameter is not a Boolean
     * @throws {Error} Throws error when addHtmlElement parameter is not a Boolean (if the HTML serialization is required)
     * @throws {Error} Throws error when addHeadElement parameter is not a Boolean (if the HTML serialization is required)
     * @throws {Error} Throws error when addBodyElement parameter is not a Boolean (if the HTML serialization is required)
     * @param {Object} options Options for the request object and the found components
     * - AKNDom - returns the XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
     *     Document</a> serialization of the components contained in the document
     * - AKNString - returns the String serialization of the components contained in the document
     * - HTMLDom - returns the HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
     *     Document</a> serialization of the components contained in the document
     * - HTMLString - returns the HTML string serialization of the components contained in the document
     * - JSON - returns the <a target="_blank" href="https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/JSON">
     *     JSON</a> serialization of the components contained in the document
     * - JSONString - returns the JSON string serialization of the components contained in the document
     * @param {String} [options.insertDataXPath=false] - Set it to true to add the data-akomando-xpath attribute to each of the returned elements
     * @param {String} [options.detailedInfo=true] - If it is set to false, the function returns only an array containing the components in the requested serialization
     * @param {String} [options.addHtmlElement=true] - Set it to true to add the html element into the created document (if the HTML serialization is required)
     * @param {String} [options.addHeadElement=true] - Set it to true to add the head element into the created document (if the HTML serialization is required)
     * @param {String} [options.addBodyElement=true] - Set it to true to add the body element into the created document (if the HTML serialization is required)
     * Document</a> serialization of the AkomaNtoso in which the research must be performed
     * @param {Object} [options.config] The configuration of the akomando according to {@link akomando.config} specications.
     * @return {Object} An object structured as a {@link akomando.components.getComponents} object and containing all compoments in the requested serialization
   */
   getComponents({
      serialize = 'AKNDom',
      detailedInfo = true,
      insertDataXPath = false,
      addHtmlElement = true,
      addHeadElement = true,
      addBodyElement = true,
   } = {}){
      if(typeCheck('Undefined',this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      if(!typeCheck('Boolean', detailedInfo)){
         throw new Error(`detailedInfo - ${detailedInfo} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('Boolean', insertDataXPath)){
         throw new Error(`insertDataXPath - ${insertDataXPath} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('Boolean', addHtmlElement)){
         throw new Error(`addHtmlElement - ${addHtmlElement} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('Boolean', addHeadElement)){
         throw new Error(`addHeadElement - ${addHeadElement} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('Boolean', addBodyElement)){
         throw new Error(`addBodyElement - ${addBodyElement} - ${errors._InputIsNotABoolean}`);
      }
      if(serialize != 'AKNDom' &&
         serialize != 'AKNString' &&
         serialize != 'HTMLDom' &&
         serialize != 'HTMLString' &&
         serialize != 'JSON' &&
         serialize != 'JSONString' &&
         serialize != 'TEXT'){
         throw new Error(`${serialize} - ${errors._SerializationNotSupported}`);
      }
      const results = _ComponentsHandler.getComponents({
         serialize,
         namespace: this.AKNDomNamespace,
         AKNDom: this._getAKNDom(),
         config: this._AKNConfig,
         detailedInfo,
         insertDataXPath,
         addHtmlElement,
         addHeadElement,
         addBodyElement,
      });
      return results;
   }

   /**
    * Returns an akomando.hier.identifiers object containing all the identifiers of all the partitions of the loaded AkomaNtoso document
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when sortBy parameter is not a String
    * @throws {Error} Throws error when the requested sorting is not supported
    * @throws {Error} Throws error when order parameter is not a String
    * @throws {Error} Throws error when the requested ordering is not supported
    * @throws {Error} Throws error when filterByName parameter is not a String
    * @throws {Error} Throws error when filterByContent parameter is not a Boolean
    * @param {Object} options The options for resulting {@link akomando.hier.identifiers} object
    * @param {String} [options.sortyBy=position] The field on wich results must be sorted. Possible values are:
    *
    * - position - The position of the element insiede the document
    * - name - The name of the element
    * - eId - The eId of the element
    * - wId - The wId of the element
    * - GUID - The GUID of the element
    * - xpath - The xpath of the element
    * - level - The level of the element
    *
    * @param {String} [options.order=ascending] The ordering of the returnet elements. Possible values are:
    *
    * - ascending - Results are ordered in ascending way
    * - descending - Results are ordered in descending way
    *
    * @param {String} [options.filterByName=null] Returns only the hierarchies having the given name
    * @param {Boolean} [options.filterByContent=false] Returns only the hierarchies having containing text (the last level partitions in hierarchies)
    * @return {Object} an {@link akomando.hier.identifiers} object containing all the identifiers of all the partitions
   */
   getHierIdentifiers({
      sortBy = 'position',
      order = 'ascending',
      filterByName = null,
      filterByContent = false,
   }={}){
      if(typeCheck('Undefined',this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      if(!typeCheck('Boolean',filterByContent)){
         throw new Error(`filterByContentBoolean - ${filterByContent} - ${errors._InputIsNotABoolean}`);
      }
      if(!typeCheck('String',order)){
         throw new Error(`order - ${order} - ${errors._InputIsNotAString}`);
      }
      if(filterByName && !typeCheck('String',filterByName)){
         throw new Error(`filterByName - ${filterByName} - ${errors._InputIsNotAString}`);
      }
      if(order != 'ascending' &&
         order != 'descending'){
         throw new Error(`${order} - ${errors._OrderingNotSupported}`);
      }
      if(sortBy != 'position' &&
         sortBy != 'name' &&
         sortBy != 'eId' &&
         sortBy != 'wId' &&
         sortBy != 'GUID' &&
         sortBy != 'xpath' &&
         sortBy != 'level'){
         throw new Error(`${sortBy} - ${errors._SortingNotSupported}`);
      }
      return  _HiersHandler.getHiersIdentifiers({
         config: aknConfig,
         AKNDom: this._getAKNDom(),
         docType: this.getDocType(),
         prefix: this._AKNDomPrefix,
         sortBy,
         order,
         filterByName,
         filterByContent,
      });
   }

   /**
    * Returns the number of elements contained in the AkomaNtoso document as a {@link akomando.elements.count} Object
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when filterByName parameter is not a String
    * @param {Object} options The options for restricting the number of elements that must be counted
    * @param {String} [options.filterByName=null] Counts only the elements having the given name
    * @return {Object} The number of elements contained in the AkomaNtoso document as a {@link akomando.elements.count} Object
   */
   countDocElements({
      filterByName = null,
   }={}){
      if(typeCheck('Undefined', this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      if(filterByName && !typeCheck('String',filterByName)){
         throw new Error(`filterByName - ${filterByName} - ${errors._InputIsNotAString}`);
      }
      let elementsInDocument = _getDocumentElementsIdentifier(
         this._getAKNDom(),
         filterByName
      );
      elementsInDocument = (filterByName)
      ?
      _.where(elementsInDocument, {
         name: filterByName,
      })
      :
      elementsInDocument;

      let result = {
         total: elementsInDocument.length,
         elements : [],
      };
      const elementNamesInDocument =   _(elementsInDocument)
                                       .chain()
                                       .flatten()
                                       .sortBy('name')
                                       .pluck('name')
                                       .unique()
                                       .value();

      for (const element of elementNamesInDocument){
         let elementsWithName = _.where(elementsInDocument, {
            name: element,
         });
         result.elements.push({
            name: element,
            count: elementsWithName.length,
         });
      }
      return result;
   }

   /**
    * Returns a list of the references that are contained in the metadata section of the document.
    * It also returns information about the references and information about the elements
    * of the document that are linked to them.
    * Results is a JSON structured as a {@link akomando.references.getReferencesInfo}
    * @protected
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when filterBy parameter is not a String
    * @throws {Error} Throws error when the requested filtering is not supported
    * @param {Object} [options] The options for resulting {@link akomando.references.getReferencesInfo} object
    * @param {String} [options.filterBy=all] Specifies how results must be filtered. Possible values are:
    *
    * - all - returns all the references
    * - used - returns only the references that are used by at least an element in the document
    * - unused - returns only the references that are not used by any element in the document
    * @return {Object} The list of elements that have a "source" attribute as
    * a {@link akomando.references.getReferencesInfo} Object
   */
   getReferencesInfo({
      filterBy = 'all',
   }={}){
      if(typeCheck('Undefined', this._getAKNDom())){
         throw new Error(errors._AKNDocumentIsNotSet);
      }
      if(filterBy && !typeCheck('String',filterBy)){
         throw new Error(`filterByName - ${filterBy} - ${errors._InputIsNotAString}`);
      }
      if(filterBy != 'all' &&
         filterBy != 'used' &&
         filterBy != 'unused'){
         throw new Error(`${filterBy} - ${errors._FiteringNotSupported}`);
      }
      const referencesInfo = _ReferencesHandler.getReferencesInfo({
         namespace: this.AKNDomNamespace,
         AKNDom: this._getAKNDom(),
         config: this._AKNConfig,
         filterBy,
      });
      return referencesInfo;
   }
}
