#!/usr/bin/env node
// libs
import program from 'commander';
import * as fs from 'fs';
import chalk from 'chalk';
import { pd } from 'pretty-data2';


// api
import { createAkomando } from '../api/akomando.js';


/**
 * Check if a path refers to a file
 * @param {String} file The path to check
 * @return {Boolean} True if the path refers to a file
*/
const _isFile = function(file){
   try {
      fs.accessSync(file, fs.F_OK);
      const stats = fs.statSync(file);
      if (stats.isFile()) {
         return true;
      }
      else {
         return false;
      }
   }
   catch (e) {
      console.log(chalk.red.bold.underline('\nThe path you inserted does not refer to a file. If you are drunk or stunned please consider to have another beer before trying again.\n'));
      console.log(e);
      return false;
   }
};

/**
 * The function that is called when the 'akomando get-doc-type <file>' command is launched
 * @throws {Error} Throws error when the path does not refer to a file
 * @param {String} file The path of the AkomaNtoso file
 * @param {Object} options The options requested by the user
 * @param {Object} [options.config=null] a configuration file according to {@link akomando.config}
 * @return {String} The doc type of the AkomaNtoso file
*/
const _getDocType = function(file,options){
   if (_isFile(file)){
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();

      let config;
      if(options.config){
         if(_isFile(options.config)){
            const configStream = fs.readFileSync(options.config);
            config = configStream.toString();
         }
         else{
            return;
         }
      }

      const akomantoso = createAkomando({
         aknString,
         config,
      });
      const result = akomantoso.getDocType();
      console.log(chalk.green.bold(result));
      return result;
   }
};

/**
 * The function that is called when the 'akomando get-meta <file>' command is launched
 * @throws {Error} Throws error when the path does not refer to a file
 * @param {String} file The path of the AkomaNtoso file
 * @param {Object} options The options requested by the user
 * @param {Object} [options.serialize=AKNString] the serialization of the results
 * @param {Object} [options.html=true] false when, if requesting an HTML serialization, the html element is not required
 * @param {Object} [options.head=true] false when, if requesting an HTML serialization, the head element is not required
 * @param {Object} [options.body=true] false when, if requesting an HTML serialization, the body element is not required
 * @param {Object} [options.newLines=true] false when new lines are not requested in the results (useful when asking for textual version of the document)
 * @return {String} The AkomaNtoso document in the requested serialization
*/
const _getAkomaNtoso = function(file, options){
   let serialize = '';
   if(!options.serialize){
      serialize = 'AKNString';
   }
   else if(options.serialize === 'json'){
      serialize = 'JSON';
   }
   else if(options.serialize === 'html'){
      serialize = 'HTMLString';
   }
   else if(options.serialize === 'akn'){
      serialize = 'AKNString';
   }
   else if(options.serialize === 'text'){
      serialize = 'TEXT';
   }
   else{
      console.log(chalk.red.bold.underline('\nThe serialization that you requested is not supported. Supported serialization are akn, html, json. \nIf you are drunk or stunned please consider to have another beer before trying again.\n'));
   }
   if (_isFile(file)){
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();
      const addHtmlElement = options.html;
      const addHeadElement = options.head;
      const addBodyElement = options.body;
      const newLines = options.newLines;

      let config;
      if(options.config){
         if(_isFile(options.config)){
            const configStream = fs.readFileSync(options.config);
            config = configStream.toString();
         }
         else{
            return;
         }
      }

      const akomantoso = createAkomando({
         aknString,
         config,
      });
      const result = akomantoso.getAkomaNtoso({
         serialize,
         addHtmlElement,
         addHeadElement,
         addBodyElement,
         newLines,
      });
      let prettyPrintedResults = '';
      if(serialize === 'JSON'){
         prettyPrintedResults = pd.json(result);
      }
      else if (serialize === 'HTMLString' || serialize == 'AKNString'){
         prettyPrintedResults = pd.xml(result.toString());
      }
      else if (serialize === 'TEXT'){
         prettyPrintedResults = result.toString();
      }
      console.log(chalk.green.bold(prettyPrintedResults));
      return prettyPrintedResults;
   }
};

/**
 * The function that is called when the 'akomando get-meta <file>' command is launched
 * @throws {Error} Throws error when the path does not refer to a file
 * @throws {Error} Throws error when the confing does not refer to a file
 * @throws {Error} Throws error when the serialization is not supported
 * @param {String} file The path of the AkomaNtoso file
 * @param {Object} options The options requested by the user
 * @param {Object} [options.serialize=AKNString] the serialization of the results
 * @param {Object} [options.dataXpath=true] when true, it inserts the xpath to eleents contained in the results
 * @param {Object} [options.html=true] false when, if requesting an HTML serializatiom, the html element is not required
 * @param {Object} [options.head=true] false when, if requesting an HTML serializatiom, the head element is not required
 * @param {Object} [options.body=true] false when, if requesting an HTML serializatiom, the body element is not required
 * @return {String} The meta of the AkomaNtoso file
*/
const _getMeta = function(file, options){
   let serialize = '';
   if(!options.serialize){
      serialize = 'AKNString';
   }
   else if(options.serialize === 'json'){
      serialize = 'JSON';
   }
   else if(options.serialize === 'html'){
      serialize = 'HTMLString';
   }
   else if(options.serialize === 'akn'){
      serialize = 'AKNString';
   }
   else{
      console.log(chalk.red.bold.underline('\nThe serialization that you requested is not supported. Supported serialization are akn, html, json. \nIf you are drunk or stunned please consider to have another beer before trying again.\n'));
   }
   if (_isFile(file)){
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();
      const addHtmlElement = options.html;
      const addHeadElement = options.head;
      const addBodyElement = options.body;
      const insertDataXPath = options.dataXpath;

      let config;
      if(options.config){
         if(_isFile(options.config)){
            const configStream = fs.readFileSync(options.config);
            config = configStream.toString();
         }
         else{
            return;
         }
      }

      const akomantoso = createAkomando({
         aknString,
         config,
      });
      const result = akomantoso.getMeta({
         serialize,
         metaRoot: options.metaRoot,
         insertDataXPath,
         addHtmlElement,
         addHeadElement,
         addBodyElement,
      });
      let prettyPrintedResults = '';
      if(serialize === 'JSON'){
         prettyPrintedResults = pd.json(result);
      }
      else if (serialize === 'AKNString' || serialize == 'HTMLString'){
         prettyPrintedResults = pd.xml(result.toString());
      }
      console.log(chalk.green.bold(prettyPrintedResults));
      return prettyPrintedResults;
   }
};

/**
 * The function that is called when the 'akomando get-components <file>' command is launched
 * @throws {Error} Throws error when the path does not refer to a file
 * @throws {Error} Throws error when the confing does not refer to a file
 * @throws {Error} Throws error when the serialization is not supported
 * @param {String} file The path of the AkomaNtoso file
 * @param {Object} options The options requested by the user
 * @param {Object} [options.serialize=AKNString] the serialization of the results
 * @param {String} [options.detailedInfo=true] - If it is set to false, the function returns only an array containing the components in the requested serialization
 * @param {Object} [options.dataXpath=true] when true, it inserts the xpath to elements contained in the results
 * @param {Object} [options.html=true] false when, if requesting an HTML serializatiom, the html element is not required (if the HTML serialization is required)
 * @param {Object} [options.head=true] false when, if requesting an HTML serializatiom, the head element is not required (if the HTML serialization is required)
 * @param {Object} [options.body=true] false when, if requesting an HTML serializatiom, the body element is not required (if the HTML serialization is required)
 * @return {Array} An array that contains the information about the components contained in the document. The array contains an object structured as {@link akomando.components.getComponents}
*/
const _getComponents = function(file, options){
   let serialize = '';
   if(!options.serialize){
      serialize = 'AKNString';
   }
   else if(options.serialize === 'json'){
      serialize = 'JSON';
   }
   else if(options.serialize === 'html'){
      serialize = 'HTMLString';
   }
   else if(options.serialize === 'akn'){
      serialize = 'AKNString';
   }
   else{
      console.log(chalk.red.bold.underline('\nThe serialization that you requested is not supported. Supported serialization are akn, html, json. \nIf you are drunk or stunned please consider to have another beer before trying again.\n'));
   }
   if (_isFile(file)){
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();
      const addHtmlElement = options.html;
      const addHeadElement = options.head;
      const addBodyElement = options.body;
      const insertDataXPath = options.dataXpath;
      const detailedInfo = options.detailedInfo;

      let config;
      if(options.config){
         if(_isFile(options.config)){
            const configStream = fs.readFileSync(options.config);
            config = configStream.toString();
         }
         else{
            return;
         }
      }

      const akomantoso = createAkomando({
         aknString,
         config,
      });
      let result = akomantoso.getComponents({
         serialize,
         detailedInfo,
         insertDataXPath,
         addHtmlElement,
         addHeadElement,
         addBodyElement,
      });
      if(!detailedInfo){
         result = result[0].components;
      }

      let prettyPrintedResults = '';
      if(serialize === 'JSON'){
         prettyPrintedResults = pd.json(result);
      }
      else if (serialize === 'AKNString' || serialize == 'HTMLString'){
         (detailedInfo) ?
            prettyPrintedResults = JSON.stringify(result) :
            prettyPrintedResults = result.map( doc => (
               pd.xml(doc)
            ));
      }
      console.log(chalk.green.bold(prettyPrintedResults));
      return prettyPrintedResults;
   }
};

/**
 * Returns all ids of all hierarchies of the loaded AkomaNtoso document
 * @protected
 * @throws {Error} Throws error when the path does not refer to a file
 * @throws {Error} Throws error when the configuration does not refer to a file
 * @throws {Error} Throws error when the requested sorting is not supported
 * @throws {Error} Throws error when the requested ordering is not supported
 * @throws {Error} Throws error when filterByContent parameter is not a Boolean
 * @param {String} file The path of the AkomaNtoso file
 * @param {Object} [options] The options for resulting {@link akomando.hier.identifiers} object
 * @param {String} [options.sortyBy=position] The field on wich results must be sorted. Possible values are:
 *
 * - position - The position of the element insiede the document
 * - name - The name of the element
 * - eId - The eId of the element
 * - wId - The wId of the element
 * - GUID - The guid of the element
 * - xpath - The xpath of the element
 * - level - The level of the element
 *
 * @param {String} [options.order=ascending] The ordering of the returnet elements. Possible values are:
 *
 * - ascending - Results are ordered in ascending way
 * - descending - Results are ordered in descending way
 *
 * @param {String} [options.filterByName=null] Returns only the hierarchies having the given name
 * @param {Boolean} [options.filterByContent=false] Returns only the hierarchies having containing text (the last level partitions in hierarchies)
 * @return {Object} an {@link akomando.hier.identifier} object containing all the identifiers of all the partitions
*/
const _getHierIdentifiers = function(file, options){
   let sortBy = '';
   let order = '';
   let filterByContent = false;
   if(!options.sortBy){
      sortBy = 'position';
   }
   else if(options.sortBy != 'position' &&
      options.sortBy != 'position' &&
      options.sortBy != 'name' &&
      options.sortBy != 'eId' &&
      options.sortBy != 'wId' &&
      options.sortBy != 'GUID' &&
      options.sortBy != 'xpath' &&
      options.sortBy != 'level'){
      console.log(chalk.red.bold.underline('\nThe sorting that you requested is not supported. Supported sorting are position, name, eId, wId, GUID, xpath, level.\nIf you are drunk or stunned please consider to have another beer before trying again.\n'));
   }
   else{
      sortBy = options.sortBy;
   }
   if(!options.order){
      order = 'ascending';
   }
   else if(options.order != 'ascending' &&
      options.order != 'descending'){
      console.log(chalk.red.bold.underline('\nThe ordering that you requested is not supported. Supported ordering are ascending and descending.\nIf you are drunk or stunned please consider to have another beer before trying again.\n'));
   }
   else{
      order = options.order;
   }
   if(options.filterByContent){
      filterByContent = true;
   }
   if (_isFile(file)){
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();
      let config;
      if(options.config){
         if(_isFile(options.config)){
            const configStream = fs.readFileSync(options.config);
            config = configStream.toString();
         }
         else{
            return;
         }
      }

      const akomantoso = createAkomando({
         aknString,
         config,
      });
      const result = akomantoso.getHierIdentifiers({
         sortBy,
         order,
         filterByName: options.filterByName,
         filterByContent,
      });
      console.log(chalk.green.bold(pd.json(result)));
      return pd.json(result);
   }
};

/**
 * Returns the number of elements contained in the AkomaNtoso document as a {@link akomando.elements.count} Object
 * @throws {Error} Throws error when the AkomaNtoso document is not set
 * @throws {Error} Throws error when filterByName parameter is not a String
 * @param {Object} options The options for restricting the number of elements that must be counted
 * @param {String} [options.filterByName=null] Counts only the elements having the given name
 * @return {Object} The number of elements contained in the AkomaNtoso document as a {@link akomando.elements.count} Object
*/
const _countDocElements = function(file, options){
   if (_isFile(file)){
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();

      let config;
      if(options.config){
         if(_isFile(options.config)){
            const configStream = fs.readFileSync(options.config);
            config = configStream.toString();
         }
         else{
            return;
         }
      }

      const akomantoso = createAkomando({
         aknString,
         config,
      });
      const result = akomantoso.countDocElements({
         filterByName: options.filterByName,
      });
      console.log(chalk.green.bold(pd.json(result)));
      return pd.json(result);
   }
};


/**
 * Returns a list of the references that are contained in the metadata section of the document.
 * It also returns information about the references and information about the elements
 * of the document that are linked to them.
 * Results is a JSON structured as a {@link akomando.find.references}
 * @protected
 * @throws {Error} Throws error when the AkomaNtoso document is not set
 * @throws {Error} Throws error when filterBy parameter is not a String
 * @throws {Error} Throws error when the requested filtering is not supported
 * @param {Object} [options] The options for resulting {@link akomando.find.references} object
 * @param {String} [options.filterBy=all] Specifies how results must be filtered. Possible values are:
 *
 * - all - returns all the references
 * - used - returns only the references that are used by at least an element in the document
 * - unused - returns only the references that are not used by any element in the document
 * @return {Object} The list of elements that have a "source" attribute as
 * a {@link akomando.checks.references} Object
*/
const _getReferencesInfo = function(file, options){
   if (_isFile(file)){
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();

      let config;
      if(options.config){
         if(_isFile(options.config)){
            const configStream = fs.readFileSync(options.config);
            config = configStream.toString();
         }
         else{
            return;
         }
      }

      const akomantoso = createAkomando({
         aknString,
         config,
      });
      const result = akomantoso.getReferencesInfo({
         filterBy: options.filterBy,
      });
      console.log(chalk.green.bold(pd.json(result)));
      return pd.json(result);
   }
};


program
   .version('0.0.2');
program
   .command('get-doc-type <file>')
   .option('-c, --config <config>', 'the configuration file')
   .description('Returns the doc type of the AkomaNtoso document.')
   .action(_getDocType);
program
   .command('get-akomantoso <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('-s, --serialize <serialize>', 'the requested serialization. Supported serialization are akn, html, json and text')
   .option('--no-html', 'when html serialization is requested, specifies if the html element must be inserted in the html')
   .option('--no-head', 'when html serialization is requested, specifies if the head element must be inserted in the html')
   .option('--no-body', 'when html serialization is requested, specifies if the body element must be inserted in the html')
   .option('--no-new-lines', 'when text serialization is requested, specifies if the newl lines in the original document must preserved or not preserved')
   .description('Returns the loaded AkomaNtoso document serialized as xml, html or json')
   .action(_getAkomaNtoso);
program
   .command('get-meta <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('-s, --serialize <serialize>', 'the requested serialization. Supported serialization are akn, html and json')
   .option('-m, --meta-root <metaRoot>', 'the meta elment to retrieve')
   .option('--no-data-xpath', 'specifies if the data-akomando-xpath attribute must be inserted in the returned elements')
   .option('--no-html', 'when html serialization is requested, specifies if the html element must be inserted in the html')
   .option('--no-head', 'when html serialization is requested, specifies if the head element must be inserted in the html')
   .option('--no-body', 'when html serialization is requested, specifies if the body element must be inserted in the html')
   .description('Returns meta of the AkomaNtoso document.')
   .action(_getMeta);
program
   .command('get-components <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('-s, --serialize <serialize>', 'the requested serialization. Supported serialization are akn, html and json')
   .option('--no-detailed-info', 'if this parameter is set, the command returns an array containing just the components found in the document in the requested serialization')
   .option('--no-data-xpath', 'specifies if the data-akomando-xpath attribute must be inserted in the returned elements')
   .option('--no-html', 'when html serialization is requested, specifies if the html element must be inserted in the html')
   .option('--no-head', 'when html serialization is requested, specifies if the head element must be inserted in the html')
   .option('--no-body', 'when html serialization is requested, specifies if the body element must be inserted in the html')
   .description('Returns a string representation of a JSON object containing all the components contained in the AkomaNtoso document.')
   .action(_getComponents);
program
   .command('get-hier-identifiers <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('--sort-by <sort>', 'the requested sorting. Supported sorting are position, name, eId, wId, GUID, xpath, level. Default sorting is position')
   .option('--order <ordering>', 'the requested ordering. Supported ordering are descending and ascending. Default ordering is ascending')
   .option('--filter-by-name <name>', 'set it to return only the hierarchies having the given name')
   .option('--filter-by-content', 'returns only the hierarchy having content')
   .description('Returns a JSON string containing all the identifiers of hierarchies contained in the file')
   .action(_getHierIdentifiers);
program
   .command('count-doc-elements <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('--filter-by-name <name>', 'set it to return only the elements having the given name')
   .description('Returns a JSON string containing the total number of elements contained in the document and the total number of elements for each type')
   .action(_countDocElements);
program
   .command('get-references-info <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('--filter-by <filter>', 'the requested filtering. Supported filtering are: all (returns all the references), used (returns only references that are used by at last one element in the document), unused (returns only the references that are not used by any element in the document). Default filtering is all')
   .description('Returns a list of the references that are contained in the metadata section of the document. It also returns information about the references and information about the elements of the document that are linked to them.')
   .action(_getReferencesInfo);

program.parse(process.argv);
if (program.args.length === 0) program.help();


